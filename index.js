// console.log("Hello Batch 245");

let userName; 
let password;
let role;

userName = prompt("Enter username: ");
password = prompt("Enter password: ");
role = prompt("Enter role: ");

function logIn(){
	if (userName == "" || userName == null || password == "" || password == null || role == "" || role == null){
		alert("Input should not be empty.");
	}
	else {
		switch(role){
			case "admin":
				alert("Welcome back to the class portal, admin!");
				break;
			case 'teacher':
				alert("Thank you for logging in, teacher!");
				break;
			case "student":
				alert("Welcome to the class portal, student!");
				break;
			default:
					alert("Role out of range.");
				break;	
		}
	}
}

logIn();


function checkAverage (number1, number2, number3, number4){
		let  average = Math.round((number1 + number2 + number3 + number4)/4);
		// console.log(numAverage);
		
		if (average	<= 74){
			console.log("Hello, student, your average is "+ average	+". The letter equivalent is F");
		}
		else if (average >=75 && average <= 79){
			console.log("Hello, student, your average is "+ average	+". The letter equivalent is D");
		}
		else if (average >=80 && average <= 84){
			console.log("HHello, student, your average is "+ average +". The letter equivalent is C");
		}
		else if (average >=85 && average <= 89){
			console.log("Hello, student, your average is "+ average +". The letter equivalent is B");
		}
		else if (average >=90 && average <= 95){
			console.log("Hello, student, your average is "+ average +". The letter equivalent is A");
		}
		else if (average >=96){
			console.log("Hello, student, your average is "+ average +". The letter equivalent is A+");
		}

}

console.log("checkAverage(71,70,73,74)");
checkAverage(71,70,73,74);

console.log("checkAverage(75,75,76,78)");
checkAverage(75,75,76,78);

console.log("checkAverage(80,81,82,78)");
checkAverage(80,81,82,78);

console.log("checkAverage(84,85,87,88)");
checkAverage(84,85,87,88);

console.log("checkAverage(89,90,91,90)");
checkAverage(89,90,91,90);

console.log("checkAverage(91,96,97,95)");
checkAverage(91,96,97,95);

